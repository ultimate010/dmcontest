# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
from keras.models import Model
from keras.layers import Input, Dense, Activation, Dropout
from keras.layers.merge import Concatenate
from keras.callbacks import ModelCheckpoint
from keras import optimizers
import numpy as np
import time

import tensorflow as tf
from keras import backend as K

# don't use all memory
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.2)  
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))  
# config = tf.ConfigProto()  
# config.gpu_options.allow_growth=True
# sess = tf.Session(config=config)
K.set_session(sess)


batch_size = 512
input_shape = [84299, 222]
checkpoint = ModelCheckpoint('model1-weights1.{epoch:03d}-{val_loss:.4f}.hdf5', monitor='val_loss', verbose=1, save_best_only=False)
callback_list = [checkpoint, ]


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

class BaseModel(object):
    @classmethod
    def build_model(cls):
        x_1 = Input(shape=(input_shape[0], ))
        x_2 = Input(shape=(input_shape[1], ))
        # x_3 = Input(shape=(500, ))
        # x_4 = Input(shape=(100, ))
        # x_5 = Input(shape=(500, ))

        y_1 = Dense(150, kernel_initializer='he_normal', activation='relu')(x_1)
        y_2 = Dense(150, activation='relu')(x_2)
        # y_3 = Dense(150, activation=Activation('relu'))(x_3)
        # y_4 = Dense(150, activation=Activation('relu'))(x_4)
        # y_5 = Dense(150, activation=Activation('relu'))(x_5)

        z_1 = Dense(50, activation='relu',)(y_1)
        z_1 = Dropout(0.1)(z_1)
        z_2 = Dense(50, activation='relu',)(y_2)
        z_2 = Dropout(0.1)(z_2)
        # z_3 = Dense(50, activation=Activation('relu'))(y_3)
        # z_4 = Dense(50, activation=Activation('relu'))(y_4)
        # z_5 = Dense(50, activation=Activation('relu'))(y_5)

        r = Concatenate()([z_1, z_2])
        # r = Concatenate()([z_1, z_2, z_3, z_4, z_5])
        s = Dense(100, activation='relu')(r)
        t = Dense(2, activation='softmax')(s)
        model = Model(inputs=[x_1, x_2], outputs=t)
        adam = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
        # model.compile(optimizer=adam, loss='categorical_crossentropy', metrics=['accuracy'])
        model.compile(optimizer=adam, loss='categorical_crossentropy')
        model.summary()
        return model


def dummy_data():
    batch_features = np.zeros((64, 500 + 500 + 500 + 100 + 500), dtype=np.float32)  # 裁剪，只取下半部分
    batch_labels = np.zeros((64, 2), dtype=np.float32)
    while True:
        batch_features = [np.random.random((64, 500)), np.random.random((64, 500)),np.random.random((64, 500)),np.random.random((64, 100)),np.random.random((64, 500)) ]
        batch_labels = np.zeros((64, 2), dtype=np.float32)
        # print batch_labels
        yield (batch_features, batch_labels)

def load_data(fileName):
    global gfr
    gfr = open(fileName)
    def readBatch():
        global gfr
        count = 0
        ret = []
        while True:
            l = gfr.readline().strip()
            if l == '':
                # eprint('reload file')
                gfr = open(fileName)
                continue
            count += 1
            ret.append(l)
            if count >= batch_size:
                break
        return ret

    while True:
        batch_features = [np.zeros((batch_size, input_shape[0]), dtype=np.float32), np.zeros((batch_size, input_shape[1]), dtype=np.float32)]  # 裁剪，只取下半部分
        batch_labels = np.zeros((batch_size, 2), dtype=np.float32)
        batch = readBatch()
        for index in range(batch_size):
            line = batch[index]
            arr = line.split('#')
            apps = arr[0]
            ad = arr[1]
            label = int(arr[2])
            if apps != '':
                for k in apps.split(','):
                    k = int(k)
                    batch_features[0][index][k] = 1.0
            if ad != '':
                ad = int(ad)
                batch_features[1][index][ad] = 1.0
            batch_labels[index][label] = 1.0
        yield batch_features, batch_labels



model = BaseModel.build_model()
model.load_weights('weights1.017-0.4434.hdf5')
model.fit_generator(load_data('rawTrain.onehot'), verbose=1,
        steps_per_epoch=int(17233865/batch_size),
        callbacks=callback_list,
        validation_data=load_data('rawTrain.onehot.t1000'),
        validation_steps=int(1000/batch_size),
        epochs=500)

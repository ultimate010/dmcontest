# -*- coding: utf-8 -*-
import numpy as np
from keras.models import load_model
import sys
from ast import literal_eval as make_tuple
import tensorflow as tf
from keras import backend as K

# don't use all memory
# gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.3)
# sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
K.set_session(sess)



# import tensorflow as tf
# from keras import backend as K
# gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.2)
# sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
# K.set_session(sess)

input_shape = [56719]
batch_size = 128

def predict(model, fileName):
    global gfr
    gfr = open(fileName)
    def readBatch():
        global gfr
        count = 0
        ret = []
        finish = False
        while True:
            l = gfr.readline().strip()
            if l == '':
                finish = True
                break
            count += 1
            ret.append(l)
            if count >= batch_size:
                break
        return ret, finish

    while True:
        batch, finish = readBatch()
        instances = []
        size = len(batch)
        batch_features = np.zeros((size, input_shape[0]), dtype=np.float32)
        batch_labels = np.zeros((size, 2), dtype=np.float32)
        for index in range(size):
            input_tuple = make_tuple(batch[index])
            instanceId = input_tuple[0]
            instances.append(instanceId)
            label = input_tuple[1]
            features = input_tuple[2]
            for n, k in enumerate(features[1]):
                batch_features[index][k] = features[2][n]
        predict_label = model.predict([batch_features, batch_features])
        for i in xrange(size):
                print '{},{}'.format(instances[i], predict_label[i][1])
        if finish:
            break

model = load_model(sys.argv[1])
predict(model, sys.argv[2])

# -*- coding: utf-8 -*-
import sys
instanceId = sys.argv[1]
output = sys.argv[2]
with open(instanceId) as fr:
    with open(output) as foutput:
        for instance in fr:
            val = foutput.readline().strip().split(',')[1]
            print "{},{}".format(instance.strip(), val)

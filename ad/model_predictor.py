# -*- coding: utf-8 -*-
import numpy as np
from keras.models import load_model
import sys

import tensorflow as tf
from keras import backend as K
config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)
K.set_session(sess)

input_shape = [84299, 222]
batch_size = 512

def predict(model, fileName):
    global gfr
    gfr = open(fileName)
    def readBatch():
        global gfr
        count = 0
        ret = []
        finish = False
        while True:
            l = gfr.readline().strip()
            if l == '':
                # eprint('reload file')
                finish = True
                break
            count += 1
            ret.append(l)
            if count >= batch_size:
                break
        return ret, finish

    while True:
        batch, finish = readBatch()
        size = len(batch)
        batch_features = [
            np.zeros((size, input_shape[0]), dtype=np.float32), np.zeros((batch_size, input_shape[1]), dtype=np.float32)
        ]
        batch_labels = np.zeros((size, 2), dtype=np.float32)
        instances = []
        for index in range(size):
            line = batch[index]
            arr = line.split('#')
            instance_id = arr[0]
            instances.append(instance_id)
            apps = arr[1]
            ad = arr[2]
            label = int(arr[3])
            if apps != '':
                for k in apps.split(','):
                    if k != 'None' and k != '':
                        k = int(k)
                        batch_features[0][index][k] = 1.0
            if ad != 'None' and ad != '':
                ad = int(ad)
                batch_features[1][index][ad] = 1.0
            batch_labels[index][label] = 1.0
        predict_label = model.predict(batch_features)
	for i in xrange(len(instances)):
            print '{},{}'.format(instances[i], predict_label[i][1])
        if finish:
            break

model = load_model(sys.argv[1])
predict(model, sys.argv[2])

# -*- coding: utf-8 -*-
import numpy as np
from keras.models import load_model
import sys

import tensorflow as tf
from keras import backend as K
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.2)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
K.set_session(sess)

input_shape = [56716]
batch_size = 512

def predict(model, fileName):
    global gfr
    gfr = open(fileName)
    def readBatch():
        global gfr
        count = 0
        ret = []
        finish = False
        while True:
            l = gfr.readline().strip()
            if l == '':
                finish = True
                break
            count += 1
            ret.append(l)
            if count >= batch_size:
                break
        return ret, finish

    while True:
        batch, finish = readBatch()
        size = len(batch)
        batch_features = np.zeros((size, input_shape[0]), dtype=np.float32)
        batch_labels = np.zeros((size, 2), dtype=np.float32)
        for index in range(size):
            line = batch[index]
            arr = line.split(' ')
            label = int(arr[0])
            features = arr[1:]
            for f in features:
                f_index, val = f.split(':')
                batch_features[index][int(f_index) - 1] = float(val)
            batch_labels[index][label] = 1.0

        predict_label = model.predict(batch_features)
        for i in xrange(size):
                print '#,{}'.format(predict_label[i][1])
        if finish:
            break

model = load_model(sys.argv[1])
predict(model, sys.argv[2])

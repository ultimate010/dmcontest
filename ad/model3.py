# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
from keras.models import Model
from keras.layers import Input, Dense, Activation, Dropout
from keras.layers.merge import Concatenate
from keras.callbacks import ModelCheckpoint
from keras import optimizers
import numpy as np
import time

import tensorflow as tf
from keras import backend as K

# don't use all memory
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.2)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
# config = tf.ConfigProto()
# config.gpu_options.allow_growth=True
# sess = tf.Session(config=config)
K.set_session(sess)


batch_size = 2048
input_shape = [56716]
checkpoint = ModelCheckpoint('0921-model3-weights.{epoch:03d}-{val_loss:.4f}.hdf5', monitor='val_loss', verbose=1, save_best_only=False)
callback_list = [checkpoint, ]


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

class BaseModel(object):
    @classmethod
    def build_model(cls):
        x_1 = Input(shape=(input_shape[0], ))
        # x_2 = Input(shape=(input_shape[1], ))
        # x_3 = Input(shape=(500, ))
        # x_4 = Input(shape=(100, ))
        # x_5 = Input(shape=(500, ))

        y_1 = Dense(300, kernel_initializer='he_normal', activation='relu')(x_1)
        y_1 = Dropout(0.1)(y_1)
        # y_2 = Dense(150, activation='relu')(x_2)
        # y_3 = Dense(150, activation=Activation('relu'))(x_3)
        # y_4 = Dense(150, activation=Activation('relu'))(x_4)
        # y_5 = Dense(150, activation=Activation('relu'))(x_5)

        z_1 = Dense(100, activation='relu',)(y_1)
        z_1 = Dropout(0.1)(z_1)
        # z_2 = Dense(50, activation='relu',)(y_2)
        # z_3 = Dense(50, activation=Activation('relu'))(y_3)
        # z_4 = Dense(50, activation=Activation('relu'))(y_4)
        # z_5 = Dense(50, activation=Activation('relu'))(y_5)

        # r = Concatenate()([z_1, z_2])
        # r = Concatenate()([z_1, z_2, z_3, z_4, z_5])
        s = Dense(100, activation='relu')(z_1)
        t = Dense(2, activation='softmax')(s)
        model = Model(inputs=[x_1], outputs=t)
        adam = optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
        # model.compile(optimizer=adam, loss='categorical_crossentropy', metrics=['accuracy'])
        model.compile(optimizer=adam, loss='categorical_crossentropy')
        model.summary()
        return model


def dummy_data():
    batch_features = np.zeros((64, 500 + 500 + 500 + 100 + 500), dtype=np.float32)  # 裁剪，只取下半部分
    batch_labels = np.zeros((64, 2), dtype=np.float32)
    while True:
        batch_features = [np.random.random((64, 500)), np.random.random((64, 500)),np.random.random((64, 500)),np.random.random((64, 100)),np.random.random((64, 500)) ]
        batch_labels = np.zeros((64, 2), dtype=np.float32)
        # print batch_labels
        yield (batch_features, batch_labels)

def load_data(fileName):
    global gfr
    gfr = open(fileName)
    def readBatch():
        global gfr
        count = 0
        ret = []
        while True:
            l = gfr.readline().strip()
            if l == '':
                # eprint('reload file')
                gfr = open(fileName)
                continue
            count += 1
            ret.append(l)
            if count >= batch_size:
                break
        return ret

    while True:
        batch_features = np.zeros((batch_size, input_shape[0]), dtype=np.float32)
        batch_labels = np.zeros((batch_size, 2), dtype=np.float32)
        try:
            batch = readBatch()
            for index in range(batch_size):
                line = batch[index]
                arr = line.split(' ')
                label = int(arr[0])
                features = arr[1:]
                for f in features:
                    f_index, val = f.split(':')
                    batch_features[index][int(f_index) - 1] = float(val)
                batch_labels[index][label] = 1.0
        except Exception as e:
            if line:
                print(line)
            print(e)
        yield batch_features, batch_labels



model = BaseModel.build_model()
model.fit_generator(load_data('train_95.svm'), verbose=1,
        steps_per_epoch=int(16372172/batch_size),
        callbacks=callback_list,
        validation_data=load_data('val_05.svm'),
        validation_steps=int(861693/batch_size),
        epochs=500)

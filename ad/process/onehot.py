from __future__ import print_function
import sys
import argparse

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def oneline(line, test=False):
    arr = line.strip().split(' ')
    instance_id = None
    if test:
        instance_id = arr[0]
        arr = arr[1:]
    apps = []
    for k in arr[0].split(','):
        if k != '\\N':
            apps.append(k)
    install_apps = map(int, apps)
    ad_app = int(arr[1])
    label = int(arr[2])
    return instance_id, install_apps, ad_app, label


class IdMap(object):
    def __init__(self):
        self.map = {}
        self.count = 0
        self.arr = []

    def max_id(self):
        return self.count

    def put(self, key):
        if key not in self.map:
            self.map[key] = self.count
            self.arr.append(key)
            self.count += 1

    def get(self, key):
        return self.map.get(key)

    def dump(self, filename):
        with open(filename, 'w') as fw:
            fw.write("{}\n".format(self.count))
            for k in self.arr:
                fw.write("{} {}\n".format(k, self.map[k]))

    def load(self, filename):
        with open(filename) as fw:
            fw.readline()
            for line in fw:
                arr = line.strip().split()
                self.put(arr[0])

def fit(filename):
    apps_id = IdMap()
    ad_id = IdMap()
    count = 0
    with open(filename) as fr:
        for line in fr:
            count += 1
            if count % 10000 == 0:
                eprint('load current: %d' % count)
            _, install_apps, ad_app, label = oneline(line)
            map(apps_id.put, install_apps)
            ad_id.put(ad_app)

    count = 0
    with open(filename) as fr:
        for line in fr:
            count += 1
            if count % 10000 == 0:
                eprint('write current: %d' % count)
            _, install_apps, ad_app, label = oneline(line)
            appids = ','.join([str(apps_id.get(k)) for k in install_apps])
            adids = str(ad_id.get(ad_app))
            print('#'.join([appids, adids, str(label)]))
    apps_id.dump("apps_ids")
    ad_id.dump("ad_ids")

def transfer(filename):
    apps_id = IdMap()
    ad_id = IdMap()
    apps_id.load("apps_ids")
    ad_id.load("ad_ids")
    with open(filename) as fr:
        for line in fr:
            instance_id, install_apps, ad_app, label = oneline(line, True)
            apps = []
            for k in install_apps:
                v = str(apps_id.get(str(k)))
                if v != 'None':
                    apps.append(v)
            ad = ''
            v = str(ad_id.get(str(ad_app)))
            if v:
                ad = v
            elif v is None or v == 'None':
                eprint('fuck no ad data')
            apps = ','.join(apps)
            print('#'.join([instance_id, apps, ad, str(label)]))

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--file", required = True,
            help = "path to input dataset of images")
    ap.add_argument("-m", "--mode", required = True,
            help = "mode fit or transfer")
    args = vars(ap.parse_args())
    if args['mode'] == 'fit':
        fit(args['file'])
    elif args['mode'] == 'transfer':
        transfer(args['file'])

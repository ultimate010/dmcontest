import sys
import argparse
import random

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-if", "--inputfile", required = True,
            help = "path to onehot file")
    ap.add_argument("-tf", "--trainfile", required = True,
            help = "path to onehot file")
    ap.add_argument("-vf", "--valfile", required = True,
            help = "path to onehot file")
    ap.add_argument("-p", "--percent", required = False,
            help = "percent of val data eg: 5")
    args = vars(ap.parse_args())
    val_percent = 5
    if args.get('percent'):
        val_percent = int(args.get('percent'))
    if val_percent > 50 or val_percent < 1:
        print 'wrong percent use 5'
        val_percent = 5
    tf = open(args['trainfile'], 'w')
    vf = open(args['valfile'], 'w')
    count = 0
    with open(args['inputfile']) as fr:
        for l in fr:
            count += 1
    val_count = int(count  * (val_percent / 100.0))
    with open(args['inputfile']) as fr:
        for l in fr:
            if val_count > 0 and random.randint(0, 100) <= val_percent:
                vf.write(l)
                val_count  -= 1
            else:
                tf.write(l)
    tf.close()
    vf.close()


# ad
## 预处理以及脚本

#### ad/process/onehot.py
生成训练和测试的onehot文件

#### ad/process/randomsplit.py
按比例拆分测试集合和训练集合，默认95%训练集，剩下5%验证集。

#### ad/header.sh
对生成的数据进行排序，生成提交个格式

#### ad/upload.sh
临时中转文件用

## baseline
仅使用用户安装的app和ad app作为特征， one hot表示

#### ad/model_predict.py
加载训练好的数据，在测试文件上进行评测。

#### ad/model.py  
参考去年冠军模型

#### ad/model1.py
基于model.py，增加dropout，当前测试效果不佳，dropout阀值过大。


## 基于google 宽深推荐模型

#### ad/todo.py

# money

## baseline
